package com.calin.ktormy

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface WeatherService {

    @GET("forecast/f2b01b0e372390319530c68e5645f282/{latitude},{longitude}?units=si")
    fun getForecast(@Path("latitude") latitude: Double,
                    @Path("longitude") longitude: Double): Call<Forecast>
}