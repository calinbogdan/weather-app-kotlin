package com.calin.ktormy

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE = 5123
    }

    private lateinit var weatherService: WeatherService
    private lateinit var locationProviderClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        weatherService = Retrofit.Builder()
            .baseUrl("https://api.darksky.net")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(WeatherService::class.java)

        updateLocation()
    }

    private fun updateLocation() {
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationProviderClient.lastLocation.addOnSuccessListener { location ->
                onLocationChanged(location)
            }
        } else {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE && grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
            updateLocation()
        }
    }

    private fun onLocationChanged(location: Location) {
        updateForecast(location.latitude, location.longitude)
    }

    private fun updateForecast(latitude: Double, longitude: Double) {
        weatherService.getForecast(latitude, longitude)
            .enqueue(object : Callback<Forecast> {
                override fun onResponse(call: Call<Forecast>, response: Response<Forecast>) {
                    if (response.isSuccessful) {
                        response.body()?.let { forecast ->
                            runOnUiThread { updateView(forecast) }
                        }
                    }
                }

                override fun onFailure(call: Call<Forecast>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "An error has occurred", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun updateView(forecast: Forecast) {
        temperatureTextView.text = forecast.currently.temperature.roundToInt().toString()
        humidityTextView.text = forecast.currently.humidity.toString()
        precipitationTextView.text = forecast.currently.precipProbability.toString()
    }
}
